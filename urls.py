BASE_URL = 'https://www.rentfaster.ca/api'
CITIES_URL = BASE_URL + '/fields/cities.json?'


def FIELDS_URL(city_id: int):
    return BASE_URL + f'/fields.json?city_id={city_id}'


def SEARCH_URL(city_id: int, no_vacancy=True):
    return BASE_URL + f'/search.json?city_id={city_id}&novacancy={1 if no_vacancy else 0}&'
