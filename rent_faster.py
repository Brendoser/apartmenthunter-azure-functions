# Python Packages
import os
import math
import json
import aiohttp
import asyncio
import requests
import pandas as pd
from typing import Dict, Optional, Any
from multiprocessing.dummy import Pool, Manager
from concurrent.futures import ThreadPoolExecutor

# Local Imports
from urls import SEARCH_URL
from listing import Listing
from common import get_city_info_by_name

MANAGER = Manager()
LISTINGS = MANAGER.list()

async def get_properties_async(city_name: str, property_filter: Optional[dict] = None, dump=False) -> pd.DataFrame:
    city_info = get_city_info_by_name(city_name)
    city_id = int(city_info['city_id'])

    formatted_filter = _format_filter(**property_filter) if property_filter else ''
    search_url = SEARCH_URL(city_id) + formatted_filter + 'cur_page=0'

    listings = []
    async with aiohttp.ClientSession() as session:
        async with session.get(search_url) as response:
            json_data = await response.json()

            prop_count = int(json_data['total'])
            total_pages = math.ceil(prop_count / int(json_data['total2']))

            listings.extend(json_data['listings'])

            async def fetch_properties(url):
                async with session.get(url) as response:
                    json_data = await response.json()
                    return json_data

            tasks = [fetch_properties(SEARCH_URL(city_id) + formatted_filter + f'cur_page={page}') for page in range(1, total_pages + 1)]
            results = await asyncio.gather(*tasks)

            for result in results:
                listings.extend(result['listings'])

    return listings


def make_filter(property_type: list = None,
                beds: list = None,
                baths: list = None,
                utilities_included: list = None,
                furnishing: list = None,
                pet: list = None,
                smoking: list = None,
                parking: list = None,
                price_from: int = 0,
                price_to: int = 10000,
                home_features: list = None,
                availability: list = None,
                neighborhood: list = None,
                proximity_to: list = None,
                save=False,
                filename='filter.json') -> Dict:
    filter = {
        'property_type': property_type,
        'beds': beds,
        'baths': baths,
        'utilities_included': utilities_included,
        'furnishing': furnishing,
        'pet': pet,
        'smoking': smoking,
        'parking': parking,
        'price_from': price_from,
        'price_to': price_to,
        'home_features': home_features,
        'availability': availability,
        'neighborhood': neighborhood,
        'proximity_to': proximity_to
    }
    if save:
        with open(filename, 'w') as file:
            json.dump(filter, file, indent=4)
    return filter


def _get_url_formatted(value_list: list):
    return ','.join([value.replace(' ', '%20').replace('+', '%2B').replace('/', '%2F') for value in value_list])


def _format_filter(property_type: list = None,
                   beds: list = None,
                   baths: list = None,
                   utilities_included: list = None,
                   furnishing: list = None,
                   pet: list = None,
                   smoking: list = None,
                   parking: list = None,
                   price_from: int = 0,
                   price_to: int = 10000,
                   home_features: list = None,
                   availability: list = None,
                   neighborhood: list = None,
                   proximity_to: list = None,
                   **kwargs
                   ):
    return f'type={_get_url_formatted(property_type) if property_type else str()}&' \
           f'beds={_get_url_formatted(beds) if beds else str()}&' \
           f'baths={_get_url_formatted(baths) if baths else str()}&' \
           f'utilities_included={_get_url_formatted(utilities_included) if utilities_included else str()}&' \
           f'garage_size={_get_url_formatted(parking) if parking else str()}&' \
           f'furnishing={_get_url_formatted(furnishing) if furnishing else str()}&' \
           f'pet={_get_url_formatted(pet) if pet else str()}&' \
           f'smoking={_get_url_formatted(smoking) if smoking else str()}&' \
           f'price_range_adv%5Bfrom%5D={price_from}&' \
           f'price_range_adv%5Bto%5D={price_to}&' \
           f'home_features={_get_url_formatted(home_features) if home_features else str()}&' \
           f'availability={_get_url_formatted(availability) if availability else str()}&' \
           f'neighborhood={_get_url_formatted(neighborhood) if neighborhood else str()}&' \
           f'keywords={_get_url_formatted(proximity_to) if proximity_to else str()}&community_subtype=Community&proximity_type=location-proximity'
