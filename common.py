# Python Imports
import requests
from fuzzywuzzy import process
from typing import Dict, List, Optional, Tuple

# Local
from urls import CITIES_URL, FIELDS_URL

_CITIES = None
_FIELDS = None


def get_community_fields(city_name: str, session: Optional[requests.Session] = None) -> Dict[str, str]:
    """ Gets all community fields from the given city name.

    Args:
        city_name (str): The name of the city.
        session (requests.Session, optional): A session object to make requests from.

    Returns:
        (dict): Dict of community fields.
    """
    fields = get_city_fields_by_city_name(city_name, session)
    return fields.get('community', None)


def get_neighborhood_fields(city_name: str, session: Optional[requests.Session] = None) -> List[str]:
    """ Gets all neighborhood fields from the given city name.

    Args:
        city_name (str): The name of the city.
        session (requests.Session, optional): A session object to make requests from.

    Returns:
        (list of str): List of neighborhood fields.
    """
    fields = get_city_fields_by_city_name(city_name, session)
    return fields.get('neighborhood', None)


def get_garage_size_fields(city_name: str, session: Optional[requests.Session] = None) -> List[str]:
    """ Gets all garage_size fields from the given city name.

    Args:
        city_name (str): The name of the city.
        session (requests.Session, optional): A session object to make requests from.

    Returns:
        (list of str): List of garage_size fields.
    """
    fields = get_city_fields_by_city_name(city_name, session)
    return fields.get('garage_size', None)


def get_lease_term_fields(city_name: str, session: Optional[requests.Session] = None) -> List[str]:
    """ Gets all lease_term fields from the given city name.

    Args:
        city_name (str): The name of the city.
        session (requests.Session, optional): A session object to make requests from.

    Returns:
        (list of str): List of lease_term fields.
    """
    fields = get_city_fields_by_city_name(city_name, session)
    return fields.get('lease_term', None)


def get_pets_dogs_fields(city_name: str, session: Optional[requests.Session] = None) -> List[str]:
    """ Gets all pets_dogs fields from the given city name.

    Args:
        city_name (str): The name of the city.
        session (requests.Session, optional): A session object to make requests from.

    Returns:
        (list of str): List of pets_dogs fields.
    """
    fields = get_city_fields_by_city_name(city_name, session)
    return fields.get('pets_dogs', None)


def get_pets_cats_fields(city_name: str, session: Optional[requests.Session] = None) -> List[str]:
    """ Gets all pets_cats fields from the given city name.

    Args:
        city_name (str): The name of the city.
        session (requests.Session, optional): A session object to make requests from.

    Returns:
        (list of str): List of pets_cats fields.
    """
    fields = get_city_fields_by_city_name(city_name, session)
    return fields.get('pets_cats', None)


def get_smoking_fields(city_name: str, session: Optional[requests.Session] = None) -> List[str]:
    """ Gets all smoking fields from the given city name.

    Args:
        city_name (str): The name of the city.
        session (requests.Session, optional): A session object to make requests from.

    Returns:
        (list of str): List of smoking fields.
    """
    fields = get_city_fields_by_city_name(city_name, session)
    return fields.get('smoking', None)


def get_furnishing_fields(city_name: str, session: Optional[requests.Session] = None) -> List[str]:
    """ Gets all furnishing fields from the given city name.

    Args:
        city_name (str): The name of the city.
        session (requests.Session, optional): A session object to make requests from.

    Returns:
        (list of str): List of furnishing fields.
    """
    fields = get_city_fields_by_city_name(city_name, session)
    return fields.get('furnishing', None)


def get_property_type_fields(city_name: str, session: Optional[requests.Session] = None) -> List[str]:
    """ Gets all property type fields from the given city name.

    Args:
        city_name (str): The name of the city.
        session (requests.Session, optional): A session object to make requests from.

    Returns:
        (list of str): List of property type fields.
    """
    fields = get_city_fields_by_city_name(city_name, session)
    return fields.get('type', None)


def get_home_features_fields(city_name: str, session: Optional[requests.Session] = None) -> List[str]:
    """ Gets all home_features fields from the given city name.

    Args:
        city_name (str): The name of the city.
        session (requests.Session, optional): A session object to make requests from.

    Returns:
        (list of str): List of home_features fields.
    """
    fields = get_city_fields_by_city_name(city_name, session)
    return fields.get('home_features', None)


def get_community_features_fields(city_name: str, session: Optional[requests.Session] = None) -> List[str]:
    """ Gets all community_features fields from the given city name.

    Args:
        city_name (str): The name of the city.
        session (requests.Session, optional): A session object to make requests from.

    Returns:
        (list of str): List of community_features fields.
    """
    fields = get_city_fields_by_city_name(city_name, session)
    return fields.get('community_features', None)


def get_building_features_fields(city_name: str, session: Optional[requests.Session] = None) -> List[str]:
    """ Gets all building_features fields from the given city name.

    Args:
        city_name (str): The name of the city.
        session (requests.Session, optional): A session object to make requests from.

    Returns:
        (list of str): List of building_features fields.
    """
    fields = get_city_fields_by_city_name(city_name, session)
    return fields.get('building_features', None)


def get_utilities_included_fields(city_name: str, session: Optional[requests.Session] = None) -> List[str]:
    """ Gets all utilities_included fields from the given city name.

    Args:
        city_name (str): The name of the city.
        session (requests.Session, optional): A session object to make requests from.

    Returns:
        (list of str): List of utilities_included fields.
    """
    fields = get_city_fields_by_city_name(city_name, session)
    return fields.get('utilities_included', None)


def get_beds_fields(city_name: str, session: Optional[requests.Session] = None) -> List[str]:
    """ Gets all beds fields from the given city name.

    Args:
        city_name (str): The name of the city.
        session (requests.Session, optional): A session object to make requests from.

    Returns:
        (list of str): List of beds fields.
    """
    fields = get_city_fields_by_city_name(city_name, session)
    return fields.get('beds', None)


def get_baths_fields(city_name: str, session: Optional[requests.Session] = None) -> List[str]:
    """ Gets all baths fields from the given city name.

    Args:
        city_name (str): The name of the city.
        session (requests.Session, optional): A session object to make requests from.

    Returns:
        (list of str): List of baths fields.
    """
    fields = get_city_fields_by_city_name(city_name, session)
    return fields.get('baths', None)


def get_city_fields_by_city_name(city_name: str, session: Optional[requests.Session] = None):
    """Returns the city fields by the name of the city. Utilizes fuzzy search to return the closest
    match. Raises ValueError if no city can be found by the given name.

    Args:
        city_name:
        session:

    Returns:

    """
    global _FIELDS
    city_info = get_city_info_by_name(city_name)

    city_id = int(city_info['city_id'])

    get_fields = False if _FIELDS is not None else True
    if not get_fields:
        cached_city_id = int(_FIELDS['city']['city_id'])
        if cached_city_id != city_id:
            get_fields = True
    if get_fields:
        if not session:
            session = _get_session()
        _FIELDS = session.get(FIELDS_URL(city_id)).json()
    return _FIELDS


def get_city_info_by_name(city_name: str, session: Optional[requests.Session] = None) -> Dict[str, str]:
    """ Returns the city slug and id by the name of the city. Utilizes fuzzy search to return the closest
    match. Raises ValueError if no city can be found by the given name.

    Args:
        city_name (str): The name of the city.
        session (requests.Session, optional): A session, in which to make HTTP requests from.

    Returns:
        (dict of str): Info about the desired city.
    """
    if not session:
        session = _get_session()
    cities_list, cities = get_cities(session)

    city_name_detected = _get_city_info_by_exact_match(cities_list, city_name)
    if not city_name_detected:
        city_name_detected = _get_city_info_by_fuzzy_name(cities_list, city_name)
        if not city_name_detected:
            raise ValueError(f'Cannot find city by name: {city_name}')
    city_info = next(city for city in cities if city['city'] == city_name_detected)
    return city_info


def get_cities(session: Optional[requests.Session] = None, only_major_cities: bool = False) -> Tuple[list, list]:
    """ Returns a tuple of city names, and the city info, for all cities. Can choose whether to only return major cities.

    Args:
        session (requests.Session, optional): A session, in which to make HTTP requests from.
        only_major_cities (bool): Indicates whether to only get major cities.

    Returns:
        (tuple): A list of city names, and the corresponding info about those cities.
    """
    global _CITIES
    if not _CITIES:
        if not session:
            session = _get_session()
        _CITIES = session.get(CITIES_URL).json()['cities']
    cities = _CITIES
    if only_major_cities:
        temp_cities = cities.copy()
        for city in cities:
            if city['major'] != '1':
                temp_cities.remove(city)
        cities = temp_cities
    cities_list = [city['city'] for city in cities]
    return cities_list, cities


def _get_city_info_by_fuzzy_name(cities: List[str], city_name: str) -> Optional[str]:
    prediction = process.extractOne(city_name, cities)
    if prediction[1] < 85:
        return None
    return prediction[0]


def _get_city_info_by_exact_match(cities: List[str], city_name: str) -> Optional[str]:
    return next((city for city in cities if city.lower() == city_name.lower()), None)


def _get_session():
    return requests.Session()
