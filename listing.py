# Python Imports
from typing import Optional, Any
from dataclasses import dataclass
from collections.abc import Iterable


@dataclass
class Listing:
    ref_id: Optional[Any] = None
    title: Optional[Any] = None
    price: Optional[Any] = None
    type: Optional[Any] = None
    sq_feet: Optional[Any] = None
    availability: Optional[Any] = None
    location: Optional[Any] = None
    rented: Optional[Any] = None
    thumb: Optional[Any] = None
    thumb2: Optional[Any] = None
    slide: Optional[Any] = None
    link: Optional[Any] = None
    latitude: Optional[Any] = None
    longitude: Optional[Any] = None
    address: Optional[Any] = None
    address_hidden: Optional[Any] = None
    city: Optional[Any] = None
    province: Optional[Any] = None
    community: Optional[Any] = None
    quadrant: Optional[Any] = None
    phone: Optional[Any] = None
    preferred_contact: Optional[Any] = None
    website: Optional[Any] = None
    smoking: Optional[Any] = None
    lease_term: Optional[Any] = None
    garage_size: Optional[Any] = None
    bedrooms: Optional[Any] = None
    den: Optional[Any] = None
    baths: Optional[Any] = None
    cats: Optional[Any] = None
    dogs: Optional[Any] = None
    utilities_included: Optional[Any] = None

    def to_df(self):
        return {
            'Price': self.price,
            'Type': self.type,
            'Community': self.community,
            'Parking': self.garage_size,
            'Utilities': ','.join(self.utilities_included) if isinstance(self.utilities_included,
                                                                         Iterable) else self.utilities_included,
            'Square Feet': self.sq_feet,
            'Bedrooms': self.bedrooms,
            'Link': self.link,
            'Thumbnail': self.thumb,
            'Latitude': self.latitude,
            'Longitude': self.longitude
        }
