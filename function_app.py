# Python Imports
import os
import time
import asyncio
import logging

# Azure Imports
import azure.functions as func
from azure.cosmos import exceptions
from azure.cosmos.aio import CosmosClient
from azure.cosmos.partition_key import PartitionKey

# Local Imports
from rent_faster import get_properties_async

# Replace these values with your Cosmos DB connection information
ENDPOINT = "https://apartment-hunter-cosmodb.documents.azure.com:443/"
CREDENTIALS = os.environ['AccountKey']
DATABASE_ID = "apartment-hunter-cosmodb"
CONTAINER_ID = "properties"
PARTITION_KEY = "/city"

CITY = 'montreal'

app = func.FunctionApp()


async def insert_properties(properties: list):
    count = 0
    async with CosmosClient(ENDPOINT, credential=CREDENTIALS) as client:
        database = client.get_database_client(DATABASE_ID)
        container = database.get_container_client(CONTAINER_ID)
        for property in properties:
            # Initialize variables for retry logic
            max_retries = 3
            retries = 0
            
            while retries < max_retries:
                # Insert the document
                await container.upsert_item(property)
                
                # Verify that the document exists in the database
                query = f"SELECT * FROM c WHERE c.id = '{property['id']}'"
                items = []
                async for item in container.query_items(query=query):
                    items.append(item)
                if len(items) > 0:
                    # Document found, break out of the retry loop
                    item = items[0]
                    logging.info(f'Item id: {item["id"]}, items amount: {len(items)}')
                    count += 1
                    break
                else:
                    # Document not found, increment retry count and retry after a delay
                    retries += 1
                    print(f"Document with id '{property['id']}' was not found after insertion. Retrying ({retries}/{max_retries})...")
                    # Optionally, introduce a delay between retries
                    time.sleep(5)

            if retries == max_retries:
                # Maximum retries reached, handle the failure accordingly
                logging.info(f"Failed to insert document with id '{property['id']}' after {max_retries} retries. Skipping...")
    logging.info(f'This many should have been added: {count}')


@app.schedule(schedule="0 0 7 * * *", arg_name="myTimer", run_on_startup=True, use_monitor=False)
async def ApartmentHunterMontreal(myTimer: func.TimerRequest) -> None:
    properties = await get_properties_async('Montreal')
    
    logging.info(f'Property: {properties[0]}')
    logging.info(f'Found {len(properties)} properties!')

    for property in properties:
        property['id'] = property['ref_id']

    await insert_properties(properties)


@app.timer_trigger(schedule="0 0 7 * * *", arg_name="myTimer", run_on_startup=True, use_monitor=False) 
async def ApartmentHunterCalgary(myTimer: func.TimerRequest) -> None:
    properties = await get_properties_async('Calgary')
    
    logging.info(f'Property: {properties[0]}')
    logging.info(f'Found {len(properties)} properties!')

    for property in properties:
        property['id'] = property['ref_id']

    await insert_properties(properties)


@app.timer_trigger(schedule="0 0 7 * * *", arg_name="myTimer", run_on_startup=True, use_monitor=False) 
async def ApartmentHunterEdmonton(myTimer: func.TimerRequest) -> None:
    properties = await get_properties_async('Edmonton')
    
    logging.info(f'Property: {properties[0]}')
    logging.info(f'Found {len(properties)} properties!')

    for property in properties:
        property['id'] = property['ref_id']

    await insert_properties(properties)


@app.timer_trigger(schedule="0 0 7 * * *", arg_name="myTimer", run_on_startup=True, use_monitor=False) 
async def ApartmentHunterVancouver(myTimer: func.TimerRequest) -> None:
    properties = await get_properties_async('Vancouver')
    
    logging.info(f'Property: {properties[0]}')
    logging.info(f'Found {len(properties)} properties!')

    for property in properties:
        property['id'] = property['ref_id']

    await insert_properties(properties)


@app.timer_trigger(schedule="0 0 7 * * *", arg_name="myTimer", run_on_startup=True,
              use_monitor=False) 
async def ApartmentHunterSaskatoon(myTimer: func.TimerRequest) -> None:
    properties = await get_properties_async('Saskatoon')
    
    logging.info(f'Property: {properties[0]}')
    logging.info(f'Found {len(properties)} properties!')

    for property in properties:
        property['id'] = property['ref_id']

    await insert_properties(properties)


@app.timer_trigger(schedule="0 0 7 * * *", arg_name="myTimer", run_on_startup=True, use_monitor=False) 
async def ApartmentHunterWinnipeg(myTimer: func.TimerRequest) -> None:
    properties = await get_properties_async('Winnipeg')
    
    logging.info(f'Property: {properties[0]}')
    logging.info(f'Found {len(properties)} properties!')

    for property in properties:
        property['id'] = property['ref_id']

    await insert_properties(properties)


@app.timer_trigger(schedule="0 0 7 * * *", arg_name="myTimer", run_on_startup=True, use_monitor=False) 
async def ApartmentHunterToronto(myTimer: func.TimerRequest) -> None:
    properties = await get_properties_async('Toronto')
    
    logging.info(f'Property: {properties[0]}')
    logging.info(f'Found {len(properties)} properties!')

    for property in properties:
        property['id'] = property['ref_id']

    await insert_properties(properties)


@app.timer_trigger(schedule="0 0 7 * * *", arg_name="myTimer", run_on_startup=True, use_monitor=False) 
async def ApartmentHunterOttawa(myTimer: func.TimerRequest) -> None:
    properties = await get_properties_async('Ottawa')
    
    logging.info(f'Property: {properties[0]}')
    logging.info(f'Found {len(properties)} properties!')

    for property in properties:
        property['id'] = property['ref_id']

    await insert_properties(properties)